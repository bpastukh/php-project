<?php

declare(strict_types=1);

namespace App\Component\RdKafka;

use RdKafka\Conf;
use RdKafka\Exception;
use RdKafka\Producer as KafkaProducer;

class Producer
{
    protected ?Conf $conf;
    protected KafkaProducer $producer;

    public function __construct(Conf $conf = null)
    {
        $this->conf = $conf;
        $this->producer = new KafkaProducer($this->conf);
    }

    /**
     * @throws Exception
     */
    public function produce(string $topicName, string $message): void
    {
        $topic = $this->producer->newTopic($topicName);
        $topic->produce(RD_KAFKA_PARTITION_UA, 0, $message);
        $this->producer->poll(100);
        $flushResult = $this->producer->flush(5000);

        if (RD_KAFKA_RESP_ERR_NO_ERROR !== $flushResult) {
            throw new Exception("Can't flush message. Error code: $flushResult");
        }
    }
}
